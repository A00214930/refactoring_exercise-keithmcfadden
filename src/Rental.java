

public class Rental {
	private final Movie movie;
	private final int daysRented;
	
	public Rental(Movie amovie, int thedaysRented ) { 
		movie =amovie;
		daysRented= thedaysRented;
		
	}
	
	public Movie getMovie() {
		return movie;
	}

	public int getDaysRented() {
		
		return daysRented;
	}
	
	public double switchMethod(){
	    
	      double thisAmount = 0.0;
	      
		switch (getMovie().getPriceCode()) {
		      case Movie.REGULAR:
		        thisAmount  += 2;
		        if ( getDaysRented() > 2) {
					thisAmount += (getDaysRented() - 2 ) * 1.5;
				}
		        break;
		      case Movie.NEW_RELEASE:
		        thisAmount += getDaysRented() *3;
		        break;
		      case Movie.CHILDREN:
		        thisAmount += 1.5;
		        if (getDaysRented() > 3 ) {
					thisAmount += (getDaysRented() - 3)* 1.5;
				}
		        break;
	    }	
		return thisAmount;
	}
	
	int frequentRenterPoints(){
		int frequentRenterPoints =0;
		frequentRenterPoints++;
	    if ( (getMovie().getPriceCode() == Movie.NEW_RELEASE)
	          && getDaysRented() > 1 ) {
			frequentRenterPoints ++;
		}
	    return frequentRenterPoints;
	}

}
