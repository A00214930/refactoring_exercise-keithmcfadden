

public class Movie {
	public static final int REGULAR = 0;
	public static final int NEW_RELEASE = 1;
	public static final int CHILDREN = 2;
	private String title;
	private int priceCode;
	
	public Movie(String atitle, int apriceCode) { 
		title = atitle;
		priceCode = apriceCode;
	}
	
	public int getPriceCode() { 
		return priceCode;
	}
	public String getTitle() { 
		return title;
	}
	public void setPriceCode(int arg) { 
		priceCode = arg;
	}
}
