
import java.util.Enumeration;
import java.util.Vector;

public class Customer {

	private String name;
	private static Vector rental = new Vector();

	public Customer(String name) {
		name = name;
	}

	public void addRental(Rental arg) {
		rental.addElement(arg);
	}

	public String getName() {
		return name;
	}

	public String statement() {
		double totalAmount = 0;
		int frequentRenterPoints = 0;
		Enumeration rentals = rental.elements();
		String result = "Rental record for " + getName() + "\n";
		while (rentals.hasMoreElements()) {
			Rental each = (Rental) rentals.nextElement();

			// determine amounts for each line

			// add frequent renter points
			frequentRenterPoints += each.frequentRenterPoints();

			// show figures for this rental
			result += "\t" + each.getMovie().getTitle() + "\t"
					+ String.valueOf(each.switchMethod()) + "\n";
			totalAmount += each.switchMethod();
		}

		// add footer lines
		result += "Amount owed is " + String.valueOf(totalAmount) + "\n";
		result += "You earned " + String.valueOf(frequentRenterPoints)
				+ " frequent renter points";
		return result;
	}

}
